## Description  

The X-Tension allows an examiner to select any two files in X-Ways and quickly send them to Beyond Compare for review.  [Beyond Compare](http://www.scootersoftware.com/), from Scooter Software, is a 3rd party file comparison tool that has built-in support/viewers for the comparison of binary/hex, tab and comma separated files, graphic/image files, registry data, source code, executables, Microsoft Word/Excel, and Adobe PDF documents.  Plug-ins for additional file types can be downloaded from [here](http://www.scootersoftware.com/download.php?zz=kb_moreformats).

This X-Tension is free for both personal and commercial use and requires Microsoft's .Net Framework v3.5 and a valid license/installation for [Beyond Compare](http://www.scootersoftware.com/).

*Note: Although this X-Tension was specifically designed for use with Beyond Compare, in theory any application that takes two file names as arguments from the command line should work (i.e. program.exe file1 file2).*

## Installation  

This X-Tension was developed and tested with X-Ways Forensic v17.8 but should work with any version past v16.9.

**Steps for Installation:**

- Depending on your use, download either the xtBeyondCompare_32.dll or xtBeyondCompare_64.dll and the sample xtBeyondCompare.ini file from [here](https://bitbucket.org/4Discovery/x-ways-beyondcompare-x-tension/downloads).  
- Although this X-Tension will work from any directory, it is recommended that you place the .dll and .ini file in the same directory as X-Ways Forensics (or in the \x64 subdirectory for 64-bit versions).  The .dll and .ini file should be kept together in the same directory. A single .ini file will work for both the 32 and 64bit versions.
- Open the xtBeyondCompare.ini file with your favorite text editor and paste in the full path to BCompare.exe.  You do **not** need to put quotes around paths that contain spaces.

~~~~
	[Settings]
	BeyondComparePath=C:\Program Files (x86)\Beyond Compare 3\BCompare.exe
~~~~

If the X-Tension cannot find a valid xtBeyondCompare.ini file or if the path specified in the .ini file does not point to an existing application, the following error will appear in the X-Ways Messages window.

![X-Ways Directory Browser](https://bitbucket.org/4Discovery/x-ways-beyondcompare-x-tension/raw/master/img/xtErrorMessage.png)

## Usage  

This X-Tension is only available via the "Run X-Tensions..." right-click context menu from the X-Ways directory browser window and will only operate on two files at a time.  If more than two files are highlighted and sent to the X-Tension, only the first two files will be processed.  

Any child objects of selected files will be ignored by the X-Tension and as of X-Ways 17.8, sending child objects and excluded files is now optional.

Add the X-Tension from the "Run X-Tensions..." menu item by right-clicking the directory browser.

![X-Ways Directory Browser](https://bitbucket.org/4Discovery/x-ways-beyondcompare-x-tension/raw/master/img/xw_context_menu.png)

Click on the "+" and browse to the location of xtBeyondCompare_32.dll or xtBeyondCompare_64.dll depending on your version of X-Ways.

![Add X-Tension to X-Ways](https://bitbucket.org/4Discovery/x-ways-beyondcompare-x-tension/raw/master/img/xw_xtension_empty.png)

Once the extension is loaded, you can edit or add the location of Beyond Compare anytime from the about (...) button.  Changes will be saved in the xtBeyondCompare.ini file in the same directory as the X-Tension. If the xtBeyondCompare.ini file does not exist, it will be created by clicking save.

![Configure X-Tension](https://bitbucket.org/4Discovery/x-ways-beyondcompare-x-tension/raw/master/img/xt_about.png)

![Beyond Compare X-Tension Configuration](https://bitbucket.org/4Discovery/x-ways-beyondcompare-x-tension/raw/master/img/xt_config.png)

Once the X-Tension is configured, select any two files in the X-Ways Directory Browser, right-click and send them to the X-Tension.

![Sending Files the X-Tension](https://bitbucket.org/4Discovery/x-ways-beyondcompare-x-tension/raw/master/img/run_xtension.png)

## Samples    

**Comparison of 2 Word Documents**
![Comparison of 2 Word Documents](https://bitbucket.org/4Discovery/x-ways-beyondcompare-x-tension/raw/master/img/word_compare.png)

**Comparison of 2 Excel Documents**
![Comparison of 2 Excel Documents](https://bitbucket.org/4Discovery/x-ways-beyondcompare-x-tension/raw/master/img/excel_compare.png)

**Comparison of 2 PDF Documents**
![Comparison of 2 PDF Documents](https://bitbucket.org/4Discovery/x-ways-beyondcompare-x-tension/raw/master/img/pdf_compare.png)

**Sample Beyond Compare Report**
![Beyond Compare Report](https://bitbucket.org/4Discovery/x-ways-beyondcompare-x-tension/raw/master/img/sample_report.png)

## Change Log  

- v1.0 (07-14-2014) - Initial Release

## References  

- Uses the X-Ways C# X-Tension API I published [here](https://github.com/chadgough/x-tensions) and documentation from the [X-Ways X-Tension API](http://www.x-ways.net/forensics/x-tensions/api.html)  
